import { Component, OnInit } from '@angular/core';
import { CountriesService } from "../../services/countries.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  countries?: any = [];

  constructor(private con: CountriesService) {
  }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.con.getAll().subscribe((res: any) => {
      this.countries = res;
      console.log(this.countries);
    });
  }

}
