import { Component } from '@angular/core';
import { AuthService } from "../../services/auth.service";
import { Usuario } from "../../models/Usuario"
import { Router } from "@angular/router";
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  vUsuario = new FormControl('', [Validators.required]);
  vContrasena = new FormControl('', [Validators.required]);
  matcher = new MyErrorStateMatcher();

  user: Usuario = {
    user: '',
    pass: ''
  };

  constructor(
    private auth: AuthService,
    private router: Router,
    private _snackBar: MatSnackBar) {
  }

  acceso() {
    this.auth.acceso(this.user).subscribe(
      res => {
        console.log(res);
        localStorage.setItem('token', res.token);
        this.router.navigate(['/home']);
      },
      err => {
        this._snackBar.open(err.error.mensaje, "Error", { duration: 2000 });
      }
    )
  }

}

/** Error cuando el control no válido está sucio, tocado o enviado. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }

}
