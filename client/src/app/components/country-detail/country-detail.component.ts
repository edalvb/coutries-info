import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { CountriesService } from "../../services/countries.service";
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-country-detail',
  templateUrl: './country-detail.component.html',
  styleUrls: ['./country-detail.component.scss']
})
export class CountryDetailComponent implements OnInit {

  animal: any;
  name: any;

  country: any;
  lat: number = 5;
  long: number = 52;
  height = 300;
  width = 230;
  layerId = 'basic';
  style = `mapbox://styles/mapbox/basic-v9`;

  constructor(private route: ActivatedRoute, private con: CountriesService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.onResize();
    this.route.params.subscribe((params) => {
      this.con.getByName(params.name).subscribe((res: any) => {
        this.long = res.latlng[0];
        this.lat = res.latlng[1];
        this.country = res;
        this.changeStyle(this.layerId);
      });
    });
  }

  changeStyle(layerId: string) {
    this.style = `mapbox://styles/mapbox/${layerId}-v9`;
  }

  openDialog(): void {
    let w = this.width;
    let h = this.height;

    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: w + "px",
      height: h + "px",
      data: { lat: this.lat, long: this.long, width: w, height: h, name: this.country.name }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?: any) {
    this.height = window.innerHeight - (window.innerWidth > 640 ? 300 : 100 );
    this.width = window.innerWidth - (window.innerWidth > 640 ? 300 : 100 );
  }

}

@Component({
  selector: 'dialog-country-map.component',
  templateUrl: 'dialog-country-map.component.html',
})
export class DialogOverviewExampleDialog {

  layerId = 'basic';
  style = `mapbox://styles/mapbox/basic-v9`;
  height = 300;
  width = 230;

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.width = data.width - 50;
      this.height = data.height - 100;
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  changeStyle(layerId: string) {
    this.style = `mapbox://styles/mapbox/${layerId}-v9`;
  }

}
