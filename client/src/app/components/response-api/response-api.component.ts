import { Component, Input, HostListener } from '@angular/core';

@Component({
  selector: 'app-response-api',
  template: `
    <div align="center">
      <form class="form">

        <mat-form-field class="full-width">
          <mat-label>Respuesta</mat-label>
          <textarea disabled matInput placeholder="Respuesta" [value]="getResponse()"></textarea>
        </mat-form-field>
      </form>
    </div>`,
  styles: [`
    .form {
      min-width: 150px;
      width: 90%;
    }

    .full-width {
      width: 100%;
    }`
  ]
})
export class ResponseApiComponent {
  @Input() response: string = "";
  width = 300;

  getResponse() {
    return JSON.stringify(this.response);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?: any) {
    this.width = window.innerWidth;
  }

}
