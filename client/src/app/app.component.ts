import { Component } from '@angular/core';
import { AuthService } from "./services/auth.service";
import { Location } from "@angular/common";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private auth: AuthService, public location: Location) {}
  title = 'client';

  cerrar() {
    this.auth.cerrarSesion();
  }

  valToolbar(): boolean {
    return this.auth.acceder();
  }

}
