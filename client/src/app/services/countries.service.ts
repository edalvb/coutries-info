import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  private api = 'https://restcountries.eu/rest/v2';

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<any>(`${this.api}/all?fields=name;alpha3Code;flag;capital`);
  }

  getByName(name: string) {
    return this.http.get<any>(`${this.api}/name/${name}`).pipe(map(([res]) => res));
  }
}
