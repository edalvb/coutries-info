import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";

import { Usuario } from "../models/Usuario";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private router: Router) { }

  acceso(user: Usuario) {
    return this.http.post<any>('http://localhost:3000/acceso', user);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  acceder(): boolean {
    // Lo que esto hace es que va a retornar si existe el token en el LocalStorage devolverá true sino retornará false.
    return !!localStorage.getItem('token');
  }

  cerrarSesion() {
    this.router.navigate(['/login']);
    localStorage.clear();
  }

}
