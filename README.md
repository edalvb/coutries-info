Esta aplicación consume la API de https://restcountries.eu/. Es una aplicación donde se muestra información de paises.

El proyecto consta de dos partes: __El servidor y el cliente__

Tenemos que ejecutar el servidor para poder ingresar y probar la aplicación.

En la carpeta **server**, ejecuta el comando: `node index.js`

Y en la carpeta **client**, ejecutar el comando: `ng serve`

Luego ingresa a tu [navegador](http://localhost:4200/home)

### Las credenciales son:
**Usuario:** bartech

**Contraseña:** angular

## Login
![](https://gitlab.com/edalvb/coutries-info/-/raw/6c45e992cce48a6f00982d67c7cf4bee964a699f/client/src/assets/img/login.png)

## Home
![](https://gitlab.com/edalvb/coutries-info/-/raw/6c45e992cce48a6f00982d67c7cf4bee964a699f/client/src/assets/img/home.png)

## Detalles del pais
![](https://gitlab.com/edalvb/coutries-info/-/raw/6c45e992cce48a6f00982d67c7cf4bee964a699f/client/src/assets/img/country-details.png)

## Mapa extendido
![](https://gitlab.com/edalvb/coutries-info/-/raw/6c45e992cce48a6f00982d67c7cf4bee964a699f/client/src/assets/img/map-country.png)