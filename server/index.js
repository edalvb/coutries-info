var jwt = require('jsonwebtoken');
var express = require('express')
var bodyParser = require('body-parser');
var cors = require('cors')
var app = express()


app.use(cors())

const user = "bartech";
const pass = "angular";

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/acceso', async function (req, res) {
    if ((pass == req.body.pass) && (user == req.body.user)) {
        // Los datos que serán guardados en el token
        const payload = {
            _id: req.body.user
        }

        // devuelve un token cuando los datos del usuario son correctos
        // Firmamos el token con 1 hora de caducidad.
        const token = jwt.sign(payload, 'secret', { expiresIn: '1h' });

        // retorna un codigo de estado 200 y que devuelva el token que el usuario ha obtenido.
        res.json({ token: token })
    } else {
        res.status(401).send({ mensaje: "Credenciales incorrectas" });
    }
});


app.listen(3000, function () {
    console.log('Servidor en el puerto 3000')
})
